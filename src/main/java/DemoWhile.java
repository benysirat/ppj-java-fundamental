/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author beny.iswaril
 */
public class DemoWhile {
    public static void main(String[] args) {
        int year=0;
        double balance =10;
        double RATE=0.5;
        while(year <=20 && balance < 1000000){
            double interest = balance * RATE/100;
            balance = balance + interest;
            System.out.println("Balance:"+balance);
            year++;
        }
        
    }
  
}
