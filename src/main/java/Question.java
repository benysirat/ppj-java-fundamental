
public class Question {
private String text;
private String answer;
/**
* Constructor kosong
*/
public Question() {
text = "";
answer = "";
}
/**
* Set pertanyaan
* @param pertanyaan Pertanyaan untuk soal
*/
public void setText(String pertanyaan) {
this.text = pertanyaan;
}
/**
* Set jawabah
* @param jawaban Test jawaban yang tepat
*/
public void setAnswer(String jawaban) {
this.answer = jawaban;
}
/**
* Cek jawabah
* @param response text jawaban dari pengguna
* @return
*/
public boolean checkAnswer(String response){
return response.equals(answer);
}
/**
* Menampilkan text soal
*/

public void cetak(){
System.out.println(text);
}
}

