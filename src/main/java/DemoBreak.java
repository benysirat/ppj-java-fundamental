
import java.util.Scanner;


public class DemoBreak {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double[] data = new double[10];
        System.out.println("Tulisakan nilai-nilai siswa (max 10 data, Q untuk selesai):");
        int index=0;
        while(true){
            String nilai=in.next();
            if(nilai.equalsIgnoreCase("Q")){
                break;
            }
            //merubah string menjadi double
            double nilaiDouble = Double.parseDouble(nilai);
            data[index]= nilaiDouble;
            index=index +1;
        }
        for(double item : data){
            System.out.print(item + "-");
        }
    }
 
}
